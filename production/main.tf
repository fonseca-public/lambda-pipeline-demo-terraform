terraform {
  backend "http" {
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 2
  }
  required_version = ">= 1.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.6"
    }
  }
}

provider "aws" {
  region = local.region
}

locals {
  name   = "lambda-demo"
  region = "eu-west-3"
  tags = {
    app = "lambda-demo"
  }
}
